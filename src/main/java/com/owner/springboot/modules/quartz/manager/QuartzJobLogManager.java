package com.owner.springboot.modules.quartz.manager;

import java.util.List;

import com.owner.springboot.common.entity.Page;
import com.owner.springboot.common.entity.Query;
import com.owner.springboot.modules.quartz.entity.QuartzJobLogEntity;

/**
 * 定时任务日志
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月20日 下午11:06:56
 */
public interface QuartzJobLogManager {

	List<QuartzJobLogEntity> listForPage(Page<QuartzJobLogEntity> page, Query query);
	
	int saveQuartzJobLog(QuartzJobLogEntity log);
	
	int batchRemove(Long[] id);
	
	int batchRemoveAll();
	
}
