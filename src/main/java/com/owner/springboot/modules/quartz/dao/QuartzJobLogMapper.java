package com.owner.springboot.modules.quartz.dao;

import com.owner.springboot.modules.quartz.entity.QuartzJobLogEntity;
import org.apache.ibatis.annotations.Mapper;
import com.owner.springboot.modules.sys.dao.BaseMapper;

/**
 * 定时任务日志
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月20日 下午11:04:51
 */
@Mapper
public interface QuartzJobLogMapper extends BaseMapper<QuartzJobLogEntity> {

	int batchRemoveAll();
	
}
