package com.owner.springboot.modules.quartz.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 定时任务日志
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月20日 下午10:34:33
 */
@Data
public class QuartzJobLogEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 记录id
	 */
	private Long logId;
	
	/**
	 * 任务id
	 */
	private Long jobId;
	
	/**
	 * spring bean 名称
	 */
	private String beanName;
	
	/**
	 * 方法名
	 */
	private String methodName;
	
	/**
	 * 参数
	 */
	private String params;
	
	/**
	 * 状态，0：失败，1：成功
	 */
	private Integer status;
	
	/**
	 * 错误信息
	 */
	private String error;
	
	/**
	 * 耗时（ms）
	 */
	private Integer times;
	
	/**
	 * 创建时间
	 */
	private Timestamp gmtCreate;

	public QuartzJobLogEntity() {
		super();
	}
}
