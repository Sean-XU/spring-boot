package com.owner.springboot.modules.generator.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 数据表属性
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月28日 下午8:12:53
 */
@Data
public class TableEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 表名
	 */
	private String tableName;
	
	/**
	 * 表格备注
	 */
	private String tableComment;
	
	/**
	 * 主键
	 */
	private ColumnEntity pk;
	
	/**
	 * 表格列
	 */
	private List<ColumnEntity> columns;
	
	/**
	 * 类名，作为实例对象使用（sysUser）
	 */
	private String objName;
	
	/**
	 * 类名，作为类型使用（SysUser）
	 */
	private String className;
	
	/**
	 * 创建时间
	 */
	private Timestamp createTime;

	public TableEntity() {
		super();
	}
}
