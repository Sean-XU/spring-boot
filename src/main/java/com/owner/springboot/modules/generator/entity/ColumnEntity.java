package com.owner.springboot.modules.generator.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据表列属性
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月28日 下午8:04:40
 */
@Data
public class ColumnEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 列名
	 */
	private String columnName;
	
	/**
	 * 数据类型
	 */
	private String dataType;
	
	/**
	 * 列注释
	 */
	private String columnComment;
	
	/**
	 * 属性名，作为类属性名（userId）
	 */
	private String fieldName;
	
	/**
	 * 属性名，作为类方法名（UserId）
	 */
	private String methodName;
	
	/**
	 * 列数据类型对应java数据类型
	 */
	private String fieldType;
	
	/**
	 * 键类型标识
	 */
	private String columnKey;
	
	/**
	 * 自增标识 auto_increment
	 */
	private String extra;

	public ColumnEntity() {
		super();
	}
}
