package com.owner.springboot.modules.base.dao;

import java.util.List;

import com.owner.springboot.common.entity.Query;
import com.owner.springboot.modules.base.entity.SysAreaEntity;
import com.owner.springboot.modules.sys.dao.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 行政区域
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月18日 下午3:36:04
 */
@Mapper
public interface SysAreaMapper extends BaseMapper<SysAreaEntity> {

	List<SysAreaEntity> listAreaByParentCode(Query query);
	
	int countAreaChildren(Long areaId);
	
}
