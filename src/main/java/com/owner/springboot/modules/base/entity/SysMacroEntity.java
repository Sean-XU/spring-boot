package com.owner.springboot.modules.base.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 通用字典
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月15日 下午12:34:37
 */
@Data
public class SysMacroEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 字典id
	 */
	private Long macroId;
	
	/**
	 * 类型id
	 */
	private Long typeId;
	
	/**
	 * 类型名称
	 */
	private String typeName;
	
	/**
	 * 字典码
	 */
	private String name;
	
	/**
	 * 字典值
	 */
	private String value;
	
	/**
	 * 状态(1:显示, 0:隐藏)
	 */
	private Integer status;
	
	/**
	 * 类型(1:参数, 0:目录)
	 */
	private Integer type;
	
	/**
	 * 排序
	 */
	private Integer orderNum;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 创建时间
	 */
	private Timestamp gmtCreate;
	
	/**
	 * 修改时间
	 */
	private Timestamp gmtModified;
	
	/**
	 * ztree属性
	 */
	private Boolean open;
	
	private List<?> list;

	public SysMacroEntity() {
		super();
	}
}
