package com.owner.springboot.modules.sys.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 角色与机构对应关系
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月17日 上午10:36:09
 */
@Data
public class SysRoleOrgEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 记录id
	 */
	private Long id;

	/**
	 * 角色ID
	 */
	private Long roleId;

	/**
	 * 机构ID
	 */
	private Long orgId;

	public SysRoleOrgEntity() {
		super();
	}
}
