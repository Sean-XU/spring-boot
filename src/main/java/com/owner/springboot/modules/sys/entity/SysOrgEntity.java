package com.owner.springboot.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 组织架构
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月17日 上午10:31:09
 */
@Data
public class SysOrgEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 机构id
	 */
	private Long orgId;
	
	/**
	 * 上级机构id，一级部门为0
	 */
	private Long parentId;
	
	/**
	 * 上级机构名称
	 */
	private String parentName;
	
	/**
	 * 机构编码
	 */
	private String code;
	
	/**
	 * 机构名称
	 */
	private String name;
	
	/**
	 * 排序
	 */
	private Integer orderNum;
	
	/**
	 * 可用标识，1：可用，0：不可用
	 */
	private Integer status;
	
	/**
	 * 创建时间
	 */
	private Timestamp gmtCreate;
	
	/**
	 * 修改时间
	 */
	private Timestamp gmtModified;
	
	/**
	 * ztree属性
	 */
	private Boolean open;
	
	private List<?> list;

	public SysOrgEntity() {
		super();
	}
}
