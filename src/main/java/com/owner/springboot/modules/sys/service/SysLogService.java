package com.owner.springboot.modules.sys.service;

import java.util.Map;

import com.owner.springboot.common.entity.R;
import com.owner.springboot.modules.sys.entity.SysLogEntity;
import com.owner.springboot.common.entity.Page;

/**
 * 系统日志
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月14日 下午8:40:52
 */
public interface SysLogService {

	Page<SysLogEntity> listLog(Map<String, Object> params);
	
	R batchRemove(Long[] id);
	
	R batchRemoveAll();
	
}
