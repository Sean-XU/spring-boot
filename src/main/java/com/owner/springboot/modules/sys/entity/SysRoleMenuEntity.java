package com.owner.springboot.modules.sys.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 角色与菜单对应关系
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月10日 上午10:48:34
 */
@Data
public class SysRoleMenuEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 记录id
	 */
	private Long id;

	/**
	 * 角色ID
	 */
	private Long roleId;

	/**
	 * 菜单ID
	 */
	private Long menuId;

	public SysRoleMenuEntity() {
		super();
	}
}
