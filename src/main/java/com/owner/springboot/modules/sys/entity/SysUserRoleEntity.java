package com.owner.springboot.modules.sys.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 用户与角色对应关系
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月10日 上午10:48:55
 */
@Data
public class SysUserRoleEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 记录id
	 */
	private Long id;

	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 * 角色id
	 */
	private Long roleId;

	public SysUserRoleEntity() {
		super();
	}
}
