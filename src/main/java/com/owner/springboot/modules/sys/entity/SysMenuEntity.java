package com.owner.springboot.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 菜单
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月9日 下午11:15:17
 */
@Data
public class SysMenuEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 菜单id
	 */
	private Long menuId;
	
	/**
	 * 父级id，一级菜单为0
	 */
	private Long parentId;
	
	/**
	 * 父级菜单名称
	 */
	private String parentName;
	
	/**
	 * 菜单名称
	 */
	private String name;
	
	/**
	 * 菜单url
	 */
	private String url;
	
	/**
	 * 授权标识(多个用逗号分隔，如：user:list,user:create)
	 */
	private String perms;
	
	/**
	 * 类型(0：目录   1：菜单   2：按钮)
	 */
	private Integer type;
	
	/**
	 * 菜单图标
	 */
	private String icon;
	
	/**
	 * 排序
	 */
	private Integer orderNum;
	
	/**
	 * 创建时间
	 */
	private Timestamp gmtCreate;
	
	/**
	 * 修改时间
	 */
	private Timestamp gmtModified;
	
	/**
	 * ztree属性
	 */
	private Boolean open;
	
	private List<?> list;

	public SysMenuEntity() {
		super();
	}
}
